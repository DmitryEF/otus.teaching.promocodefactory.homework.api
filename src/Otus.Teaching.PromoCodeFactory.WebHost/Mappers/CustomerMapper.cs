﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    using Customerapi;

    public class CustomerMapper
    {
        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = string.IsNullOrEmpty(model.FirstName) ? customer.FullName : model.FirstName;
            customer.LastName = string.IsNullOrEmpty(model.LastName) ? customer.LastName : model.LastName;
            customer.Email = string.IsNullOrEmpty(model.Email) ? customer.Email : model.Email;

            var listOfPreferences = preferences?.ToList();

            if (listOfPreferences != null && listOfPreferences.ToList().Any())
            {
                customer.Preferences?.Clear();

                customer.Preferences = listOfPreferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList();
            }
            else
            {
                customer.Preferences.Clear();
            }

            return customer;
        }
        
        public static Customer MapFromModel(GrpcCreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer
                {
                    Id = Guid.NewGuid()
                };
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }
        
        public static Customer MapFromModel(GrpcUpdateCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            return MapFromModel(model.Request, preferences, customer);
        }
    }
}
