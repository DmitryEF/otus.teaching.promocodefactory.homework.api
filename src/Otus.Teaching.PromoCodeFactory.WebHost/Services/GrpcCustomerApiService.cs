﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Customerapi;
    using Google.Protobuf.WellKnownTypes;
    using Grpc.Core;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

    public class GrpcCustomerApiService : GrpcCustomers.GrpcCustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public GrpcCustomerApiService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            this._customerRepository = customerRepository;
            this._preferenceRepository = preferenceRepository;
        }

        public override async Task<ListOfCustomerShortResponce> GrpcGetCustomers(Empty request, ServerCallContext context)
        {
            var result = new ListOfCustomerShortResponce();

            result.Responces.AddRange(
                GrpcCustomerMapper.MapFromIEnumerableCustomerToRepeatedField(
                    await this._customerRepository.GetAllAsync()));

            return result;
        }
        
        public override async Task<GrpcCustomerResponce> GrpcGetCustomer(GetCustomerByIdRequest request, 
            ServerCallContext context)
        {
            if (string.IsNullOrEmpty(request?.Id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer id not provided"));

            Customer customer;

            try
            {
                customer = await this._customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            }
            catch (Exception e)
            {
                throw new RpcException(new Status(StatusCode.Internal, e.Message));
            }

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id = {request.Id} not found!"));
            
            return GrpcCustomerMapper.MapFromCustomer(customer);
        } 
        
        public override async Task<GrpcCustomerResponce> GrpcUpdateCustomer(GrpcUpdateCustomerRequest updateRequest, 
            ServerCallContext context)
        {
            if (string.IsNullOrEmpty(updateRequest?.Id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer id not provided"));

            Customer customer;

            try
            {
                customer = await this._customerRepository.GetByIdAsync(Guid.Parse(updateRequest.Id));
            }
            catch (Exception e)
            {
                throw new RpcException(new Status(StatusCode.Internal, e.Message));
            }

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id = {updateRequest.Id} not found!"));

            var preferences = 
                await _preferenceRepository.GetRangeByIdsAsync(
                    updateRequest.Request.PreferenceIds
                        .Select(Guid.Parse).ToList());

            CustomerMapper.MapFromModel(updateRequest, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return GrpcCustomerMapper.MapFromCustomer(customer);
        }

        public override async Task<GrpcCustomerResponce> GrpcCreateCustomer(GrpcCreateOrEditCustomerRequest request,
            ServerCallContext context)
        {
            try
            {
                var preferences = await this._preferenceRepository
                    .GetRangeByIdsAsync(
                        request.PreferenceIds
                            .Select(Guid.Parse).ToList());
                
                var customer = CustomerMapper.MapFromModel(request, preferences);

                await this._customerRepository.AddAsync(customer);

                return GrpcCustomerMapper.MapFromCustomer(customer);
            }
            catch (Exception e)
            {
                throw new RpcException(new Status(StatusCode.Internal, e.Message));
            }
        }

        public override async Task<Empty> GrpcDeleteCustomer(DeleteCustomerByIdRequest request,
            ServerCallContext context)
        {
            if (string.IsNullOrEmpty(request?.Id))
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Customer id not provided"));

            Customer customer;

            try
            {
                customer = await this._customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            }
            catch (Exception e)
            {
                throw new RpcException(new Status(StatusCode.Internal, e.Message));
            }

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id = {request.Id} not found!"));

            await this._customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
