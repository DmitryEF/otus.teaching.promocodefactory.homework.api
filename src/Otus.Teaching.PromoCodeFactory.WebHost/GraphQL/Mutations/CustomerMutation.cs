﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Mutations
{
    using HotChocolate;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;
    using System.Threading.Tasks;
    using System;
    using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
    using Grpc.Core;

    public class CustomerMutation
    {
        public async Task<CustomerResponse> CreateCustomer(
            CreateOrEditCustomerRequest request, 
            [Service] IRepository<Customer> customerRepository, 
            [Service] IRepository<Preference> preferencesRepository)
        {
            try
            {
                var preferences = 
                    await preferencesRepository.GetRangeByIdsAsync(request.PreferenceIds);

                var customer = CustomerMapper.MapFromModel(request, preferences);

                await customerRepository.AddAsync(customer);

                return new CustomerResponse(customer);
            }
            catch (Exception e)
            {
                throw new GraphQLException(e.Message);
            }
        }

        public async Task<CustomerResponse> UpdateCustomer(
            Guid id,
            CreateOrEditCustomerRequest request,
            [Service] IRepository<Customer> customerRepository,
            [Service] IRepository<Preference> preferencesRepository)
        {
            try
            {
                var customer = await customerRepository.GetByIdAsync(id);

                if (customer == null)
                    throw new GraphQLException($"Customer with id = {id} not found!");

                var preferences =
                    await preferencesRepository.GetRangeByIdsAsync(request.PreferenceIds);

                CustomerMapper.MapFromModel(request, preferences, customer);

                await customerRepository.UpdateAsync(customer);

                return new CustomerResponse(customer);
            }
            catch (Exception e)
            {
                throw new GraphQLException(e.Message);
            }
        }

        public async Task<bool> DeleteCustomer(
            Guid id,
            [Service] IRepository<Customer> customerRepository)
        {
            try
            {
                var customer = await customerRepository.GetByIdAsync(id);

                if (customer == null)
                    throw new GraphQLException($"Customer with id = {id} not found!");

                await customerRepository.DeleteAsync(customer);

                return true;
            }
            catch (Exception e)
            {
                throw new GraphQLException(e.Message);
            }
        }
    }
}
