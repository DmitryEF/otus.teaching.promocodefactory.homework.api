﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    using System.Collections.Generic;
    using System.Linq;
    using Customerapi;
    using Google.Protobuf.Collections;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class GrpcCustomerMapper
    {
        public static RepeatedField<GrpcCustomerShortResponce> MapFromIEnumerableCustomerToRepeatedField(
            IEnumerable<Customer> customers)
        {
            var result = new RepeatedField<GrpcCustomerShortResponce>();

            foreach (var customer in customers)
            {
                result.Add(new GrpcCustomerShortResponce()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                });
            }

            return result;
        }

        public static GrpcCustomerResponce MapFromCustomer(Customer customer)
        {
            return new GrpcCustomerResponce()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences =
                {
                    customer.Preferences.Select(x => new GrpcPreferenceResponse
                    {
                        Id = $"{x.Preference?.Id}",
                        Name = x.Preference?.Name
                    })
                }
            };
        }
    }
}