﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Queries
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using HotChocolate;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public class CustomerQuery
    {
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomers([Service] IRepository<Customer> customerRepository)
        {
            var customers = await customerRepository.GetAllAsync();

            return customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
        }

        public async Task<CustomerResponse> GetCustomer(Guid id, [Service] IRepository<Customer> customerRepository)
        {
            Customer customer;

            try
            {
                customer = await customerRepository.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new GraphQLException(e.Message);
            }

            if (customer == null)
                throw new GraphQLException($"Customer with id = {id} not found!");

            return new CustomerResponse(customer);
        }
    }
}
